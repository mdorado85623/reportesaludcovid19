function MSJERROR(){
    swal("error", "no se pudo guardar", "error");
}

function registrado(){
    swal("Perfecto", "PERFIL REGISTRADO CORRECTAMENTE", "success");
}

$("#btnenviar").click((e) => {
    e.preventDefault();
    var searchIDs = $('input:checked').map(function(){
        return $(this).val();
      });
    //   console.log(searchIDs.get());
    let nombres = $("#nombres").val();
    let apellidos = $("#apellidos").val();
    let edad = $("#edad").val();
    let estadocivil = $("#estadocivil").val();
    let genero = $("#genero").val();
    let personasacargo = $("#personasacargo").val();
    let actividades = searchIDs.get();

    $.ajax({
        type: "POST",
        url: 'backend/registroperfil.php',
        data:{nombres,apellidos,edad,estadocivil,genero,personasacargo,actividades},

        success: (data) => {
            data==="1" ? registrado() : MSJERROR();
        }
    })
})