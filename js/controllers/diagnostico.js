function MSJERROR(){
    swal("error", "no se pudo guardar", "error");
}

function registrado(){
    swal("Perfecto", "DIAGNOSTICO REGISTRADO CORRECTAMENTE", "success");
}


$("#btndiagnosticosubmit").click((e) => {
    e.preventDefault();
    var searchIDs = $('input:checked').map(function(){
        return $(this).val();
      });
      
    let comosesiente = $("#comosesiente").val();
    let temperatura = $("#temperatura").val();
    let antecedentes = $("#antecedentes").val();
    let sintomas = searchIDs.get();

    $.ajax({
        type: "POST",
        url: 'backend/registrodiagnostico.php',
        data:{comosesiente,temperatura,antecedentes,sintomas},

        success: (data) => {
            data==="1" ? registrado() : MSJERROR();
        }
    })
})


