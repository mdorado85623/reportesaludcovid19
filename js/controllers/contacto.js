function MSJERROR(){
    swal("error", "no se pudo guardar", "error");
}

function registrado(){
    swal("Perfecto", "Reporte Diario De Contacto REGISTRADO CORRECTAMENTE", "success");
}


$("#btnenviarcontacto").click((e) => {
    e.preventDefault();
    var searchIDs = $('input:checked').map(function(){
        return $(this).val();
      });
      
    let personas = $("#personas").val();
    let tiempo = $("#tiempo").val();
    let laboral = $("#laboral").val();
    let transporte = $("#transporte").val();
    let residencia = searchIDs.get();

    $.ajax({
        type: "POST",
        url: 'backend/registrocontacto.php',
        data:{personas, tiempo, laboral, transporte, residencia},

        success: (data) => {
            data==="1" ? registrado() : MSJERROR();
        }
    })
})


