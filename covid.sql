-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-12-2020 a las 05:02:08
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `covid`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administracion`
--

CREATE TABLE `administracion` (
  `usuario` varchar(30) NOT NULL,
  `clave` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administracion`
--

INSERT INTO `administracion` (`usuario`, `clave`) VALUES
('admin', '1234'),
('admin2', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `condicion_laboral` varchar(80) NOT NULL,
  `transporte` varchar(80) NOT NULL,
  `lugares_desplazado` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`lugares_desplazado`)),
  `tiempo_lugar` varchar(80) NOT NULL,
  `personas_contacto` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id`, `condicion_laboral`, `transporte`, `lugares_desplazado`, `tiempo_lugar`, `personas_contacto`) VALUES
(1, 'Estoy trabajando en casa', 'A pie', '[\" Lugar de trabajo\",\" Centro medico o EPS\",\" Taller de mecanica\"]', 'Nada (Estuve en casa)', 'Mas de 20 personas'),
(2, 'Estoy en teletrabajo', 'A pie', '[\" Areas comunes del barrio\",\" Lugar de trabajo\"]', 'Mayor a 4 horas', 'Mas de 20 personas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostico`
--

CREATE TABLE `diagnostico` (
  `id` int(11) NOT NULL,
  `como_se_siente` varchar(25) NOT NULL,
  `temperatura` int(11) NOT NULL,
  `antecedentes` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `sintomas` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`sintomas`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `diagnostico`
--

INSERT INTO `diagnostico` (`id`, `como_se_siente`, `temperatura`, `antecedentes`, `sintomas`) VALUES
(1, 'Estable', 21, 'Me realizaron la prueba y diagnosticaron POSITIVO', '[\"on\",\"on\"]'),
(2, 'Estable', 23, 'Me realizaron la prueba y diagnosticaron POSITIVO', '[\"on\",\"on\",\"on\"]'),
(3, 'Elegir', 0, '', 'null'),
(4, 'Estable', 12, 'Me realizaron la prueba y diagnosticaron POSITIVO', '[\"on\",\"on\",\"on\"]'),
(5, '', 0, '', 'null'),
(6, '', 0, '', 'null'),
(7, '', 0, '', 'null');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(35) NOT NULL,
  `edad` int(11) NOT NULL,
  `estado_civil` varchar(20) NOT NULL,
  `genero` varchar(10) NOT NULL,
  `personas_cargo` varchar(30) NOT NULL,
  `actividades` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`actividades`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`id`, `nombre`, `apellidos`, `edad`, `estado_civil`, `genero`, `personas_cargo`, `actividades`) VALUES
(4, 'Carlos', 'Pedraza', 12, 'Soltero(a)', 'femenino', '2', '[\"Consumidor activo de cigarrillo\",\"Problemas de hipertensiu00f3n arterial\",\"Problemas de asma\",\"Problemas de obesidad\"]'),
(5, 'Pedro', 'Almanza', 12, 'Soltero(a)', 'femenino', '1', '[\"Consumidor activo de cigarrillo\",\"Convive con personas fumadoras\",\"Practica actividad fisica\",\"Uso de algun tipo de corticoides o inmunosupresores\"]');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
